set(bench_SOURCES
  ${CMAKE_CURRENT_SOURCE_DIR}/bench.cpp
)

add_executable(bench ${bench_SOURCES})
target_include_directories(bench PUBLIC ${nbody_INCLUDE_DIRECTORIES})
target_link_libraries(bench PUBLIC ${nbody_LIBRARIES} -O3)
target_compile_options(bench PUBLIC ${nbody_COMPILE_OPTIONS} -O3)
target_compile_definitions(bench PUBLIC ${nbody_DEFINITIONS} HAVE_OPENMP=${HAVE_OPENMP} HAVE_OPENCL=${HAVE_OPENCL})
