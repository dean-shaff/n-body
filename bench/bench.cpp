#include <iostream>
#include <string>
#include <vector>
#include <omp.h>

#include "nbody/util/bench.hpp"
#include "nbody/util/create_parser.hpp"
#include "nbody/util/initial_conditions.hpp"
#include "nbody/engines/SingleThreadEngine.hpp"
#include "nbody/engines/SingleThreadUpperEngine.hpp"
#if HAVE_OPENMP
#include "nbody/engines/OpenMPEngine.hpp"
#endif
#if HAVE_OPENCL
#include "nbody/engines/OpenCLEngine.hpp"
#endif
#include "nbody/solvers/RungeKutta.hpp"


template<typename EngineType, typename SolverType, typename DataContainer>
nbody::util::duration benchmark (
  EngineType& engine,
  SolverType& solver,
  std::vector<DataContainer>& pos,
  std::vector<DataContainer>& vel,
  unsigned steps
)
{
  auto pos_updates = [&engine] (auto ... as) {
    engine.position_updates(as...);
  };
  auto vel_updates = [&engine] (auto ... as) {
    engine.velocity_updates(as...);
  };

  auto t_start = nbody::util::now();
  for (unsigned istep=0; istep<steps; istep++) {
    solver(
      pos_updates, vel_updates, pos.data(), vel.data());
  }
  nbody::util::duration t_delta = nbody::util::now() - t_start;
  return t_delta;
}


int main (int argc, char *argv[]) {

  using VecStr = std::vector<std::string>;

  cxxopts::Options options(
     "nbody benchmark",
     "Time different nbody engines");

  nbody::util::create_parser(options);

  options.add_options()
    ("n,bodies", "number of bodies", cxxopts::value<unsigned>()->default_value("10"));

  auto result = options.parse(argc, argv);
  if (result["help"].as<bool>()) {
    std::cerr << options.help() << std::endl;
    return 0;
  }

  // double h_val = 0.0001;
  double h_val = result["step"].as<double>();
  unsigned bodies = result["bodies"].as<unsigned>();
  unsigned steps = result["time-steps"].as<unsigned>();
  VecStr engine_names = result["engine"].as<VecStr>();

  if (engine_names.size() == 0) {
    engine_names.push_back("SingleThreadEngine");
  }

  std::vector<double> masses(bodies);
  nbody::util::generate_masses(masses);

  std::vector<nbody::Vector<double>> pos(bodies);
  std::vector<nbody::Vector<double>> vel(bodies);

  nbody::solvers::RungeKutta<2, nbody::Vector<double>> solver(h_val, masses.size());

  for (unsigned idx=0; idx<engine_names.size(); idx++) {
    std::string engine_name = engine_names[idx];
    nbody::util::duration t_delta;
    // generate same initial conditions for each run
    nbody::util::generate_initial_conditions(pos, vel);

    if (engine_name == "SingleThreadUpperEngine") {
      std::cerr << "Using SingleThreadUpperEngine" << std::endl;
      nbody::engines::SingleThreadUpperEngine<double> engine(masses);
      t_delta = benchmark(engine, solver, pos, vel, steps);
    }
    #if HAVE_OPENMP
    else if (engine_name == "OpenMPEngine") {
      int nthreads;
      #pragma omp parallel
      {
        #pragma omp single
        nthreads = omp_get_num_threads();
      }
      std::cerr << "Using OpenMPEngine with " << nthreads << " threads (" << omp_get_max_threads() << " threads max)" << std::endl;
      nbody::engines::OpenMPEngine<double> engine(masses);
      t_delta = benchmark(engine, solver, pos, vel, steps);
    }
    #endif
    #if HAVE_OPENCL
    else if (engine_name == "OpenCLEngine") {
      std::cerr << "Using OpenCLEngine" << std::endl;
      nbody::engines::OpenCLEngine<double> engine(masses);
      t_delta = benchmark(engine, solver, pos, vel, steps);
    }
    #endif
    else {
      std::cerr << "Using SingleThreadEngine" << std::endl;
      nbody::engines::SingleThreadEngine<double> engine(masses);
      t_delta = benchmark(engine, solver, pos, vel, steps);
    }

    std::cerr << engine_name << " took " << t_delta.count() << " s to compute "
      << steps << " steps for " << bodies << " bodies" << std::endl;
  }
}
