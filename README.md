## N-Body Simulation in C++/CUDA

Experiments in N-body simulations.

### Usage

```
nbody -t 1000 -s 0.0001 -e SingleThreadEngine 
```
