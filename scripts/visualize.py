import sys

import h5py
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

def load_data(file_path: str) -> np.ndarray:
    fd = h5py.File(file_path, "r")
    dat = fd["data"][...]
    fd.close()
    return dat

def visualize_data(data: np.ndarray):

    time_steps, n_masses, _ = data.shape

    fig, ax = plt.subplots(1, 1)

    def get_xy(time_step):
        return time_step[:, :2].T

    # paths = ax.scatter(*get_xy(data[0]))
    # print(dir(paths))

    xdata, ydata = [], []
    ln, = plt.plot([], [], 'ro')

    def init():
        ax.set_xlim(-2, 2)
        ax.set_ylim(-2, 2)
        return ln,

    def update(dat):
        x, y = get_xy(dat)
        xdata = x
        ydata = y
        ln.set_data(xdata, ydata)
        return ln,

    ani = FuncAnimation(fig, update, frames=data,
                        init_func=init, blit=True)

    plt.show()


if __name__ == "__main__":

    # file_path = "./../test/test_integration.hdf5"
    file_path = sys.argv[1]

    dat = load_data(file_path)
    visualize_data(dat)
