#include <vector>
#include <functional>

#include "catch.hpp"

#include "nbody/engines/SingleThreadEngine.hpp"
#include "nbody/solvers/RungeKutta.hpp"
#include "nbody/io/hdf5.hpp"

TEMPLATE_TEST_CASE(
  "Advance NBody simulation through time",
  "[integration]",
  double
)
{
  // M_0 = mass of earth,
  // R_0 = 1 au

  // V_0 = R_0/T_0
  // T_0 = (R_0^3/(G*M_0))^(1/2) where G is gravitational constant
  TestType h = 0.0001;
  using PosVelCon = nbody::Vector<TestType>; // Position Velocity Container

  unsigned nsteps = 1000;
  // Mars, Earth, Sun system
  std::vector<TestType> masses = {0.107, 1.0, 332946.0};

  nbody::engines::SingleThreadEngine<TestType> engine(masses);
  nbody::solvers::RungeKutta<2, PosVelCon> solver(h, masses.size());

  // setup initial conditions
  std::vector<PosVelCon> pos = {
    PosVelCon(-1.523, 0.0, 0.0),
    PosVelCon(-1.0, 0.0, 0.0),
    PosVelCon(0.0, 0.0, 0.0)
  };

  std::vector<PosVelCon> vel = {
    PosVelCon(0.0, 465.08, 0.0),
    PosVelCon(0.0, 576.96, 0.0),
    PosVelCon(0.0, 0.0, 0.0)
  };


  std::vector<TestType> results (6*nsteps*masses.size());

  // pre C++14
  // auto pos_updates = std::bind(
  //   &nbody::engines::SingleThreadEngine<TestType>::position_updates,
  //   &engine,
  //   std::placeholders::_1,
  //   std::placeholders::_2,
  //   std::placeholders::_3
  // );
  //
  // auto vel_updates = std::bind(
  //   &nbody::engines::SingleThreadEngine<TestType>::velocity_updates,
  //   &engine,
  //   std::placeholders::_1,
  //   std::placeholders::_2,
  //   std::placeholders::_3
  // );
  // C++14 +
  auto pos_updates = [&engine] (auto ... as) {
    engine.position_updates(as...);
  };

  auto vel_updates = [&engine] (auto ... as) {
    engine.velocity_updates(as...);
  };


  unsigned index;
  for (unsigned istep=0; istep<nsteps; istep++) {
    for (unsigned imass=0; imass<masses.size(); imass++) {
      index = 6*istep*masses.size() + 6*imass;
      results[index] = pos[imass].x;
      results[index + 1] = pos[imass].y;
      results[index + 2] = pos[imass].z;
      results[index + 3] = vel[imass].x;
      results[index + 4] = vel[imass].y;
      results[index + 5] = vel[imass].z;
    }
    solver(
      pos_updates,
      vel_updates,
      pos.data(),
      vel.data()
    );
  }

  nbody::io::hdf5::save (
    "test_integration.hdf5", "data", results.data(), {nsteps, masses.size(), 6});

}
