#include <vector>

#include "catch.hpp"

#include "nbody/solvers/RungeKutta.hpp"

TEMPLATE_TEST_CASE(
  "First Order RungeKutta solver works",
  "[RungeKutta][solvers][unit]",
  float// , double
)
{
  auto y_prime = [] (TestType* in, TestType* out) -> void { return; };

  nbody::solvers::RungeKutta<1, TestType> solver(0.1, 2);

  std::vector<TestType> y_val(2, 1.0);
  solver(y_prime, y_val.data());
}

TEMPLATE_TEST_CASE(
  "Second Order RungeKutta solver works",
  "[RungeKutta][solvers][unit]",
  float// , double
)
{
  auto y_prime = [] (TestType* pos, TestType* vel, TestType* out) -> void { return; };
  auto y_d_prime = [] (TestType* pos, TestType* vel, TestType* out) -> void { return; };

  nbody::solvers::RungeKutta<2, TestType> solver(0.1, 2);

  std::vector<TestType> pos(2, 1.0);
  std::vector<TestType> vel(2, 1.0);

  solver(y_prime, y_d_prime, pos.data(), vel.data());
}
