#include <string>

#include "catch.hpp"

#include "opencl/bridge/OpenCLBridge.hpp"

const std::string source =
#include "nbody/engines/OpenCLEngine_kernels.cl.h"
;


TEST_CASE (
  "OpenCLBridge works",
  "[unit][opencl][OpenCLBridge]"
)
{
  SECTION ("Null constructor work") {
    opencl::bridge::OpenCLBridge bridge;
  }

  SECTION ("Constructor from file path work") {
    opencl::bridge::OpenCLBridge bridge {source};
  }

  SECTION ("Copy constructor works") {
    opencl::bridge::OpenCLBridge bridge {source};
    bridge.create_kernel("calculate_accelerations");
    opencl::bridge::OpenCLBridge new_bridge = bridge;
  }

  SECTION ("Copy assignment operator works") {
    opencl::bridge::OpenCLBridge bridge {source};
    bridge.create_kernel("calculate_accelerations");
    opencl::bridge::OpenCLBridge new_bridge;
    new_bridge = bridge;
  }

  SECTION ("Move assignment operator works") {
    opencl::bridge::OpenCLBridge bridge;
    bridge = opencl::bridge::OpenCLBridge {source};
  }
}
