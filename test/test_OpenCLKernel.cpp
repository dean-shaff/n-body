#include "catch.hpp"

#include "opencl/bridge/OpenCLBridge.hpp"
#include "opencl/bridge/OpenCLKernel.hpp"

const std::string source =
#include "nbody/engines/OpenCLEngine_kernels.cl.h"
;


TEST_CASE (
  "OpenCLKernel works",
  "[unit][opencl][OpenCLKernel]"
)
{
  opencl::bridge::OpenCLBridge bridge {source};
  opencl::bridge::OpenCLKernel<
    opencl::bridge::OpenCLBridge
  > kernel = bridge.create_kernel("calculate_accelerations");
  opencl::bridge::OpenCLKernelFunctor<
    opencl::bridge::OpenCLBridge
  > functor;

  functor = kernel(1, 1);

  opencl::bridge::OpenCLKernelFunctor<
    opencl::bridge::OpenCLBridge
  > functor1 = kernel(1, 1);

}
