#include <vector>
#include <iostream>

#include "catch.hpp"

#include "nbody/io/hdf5.hpp"

TEMPLATE_TEST_CASE (
  "can save data to disk using HDF5",
  "[unit][io][hdf5][save]",
  float, double
)
{
  unsigned time_steps = 10;
  unsigned n_masses = 3;

  std::vector<TestType> result (6*time_steps*n_masses, 1.0);

  nbody::io::hdf5::save (
    "test.hdf5", "data", result.data(), {time_steps, n_masses, 6});
}

TEMPLATE_TEST_CASE (
  "can load data from disk using HDF5",
  "[unit][io][hdf5][load]",
  float, double
)
{
  unsigned time_steps = 10;
  unsigned n_masses = 3;

  std::vector<TestType> result_save (6*time_steps*n_masses, 1.0);
  std::vector<TestType> result_load (6*time_steps*n_masses, 0.0);

  std::vector<hsize_t> dims;

  nbody::io::hdf5::save (
    "test.hdf5", "data", result_save.data(), {time_steps, n_masses, 6});

  nbody::io::hdf5::load (
    "test.hdf5", "data", result_load.data(), dims);

  unsigned nclose = 0;
  for (unsigned idx=0; idx<result_load.size(); idx++) {
    if (result_load[idx] == result_save[idx]) {
      nclose++;
    }
  }
  REQUIRE(nclose == result_load.size());
  REQUIRE(dims[0] == 10);
  REQUIRE(dims[1] == 3);
  REQUIRE(dims[2] == 6);
}
