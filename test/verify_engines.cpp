#include "catch.hpp"

#include "nbody/util/initial_conditions.hpp"
#include "nbody/util/isclose.hpp"
#include "nbody/solvers/RungeKutta.hpp"
#if HAVE_OPENCL
#include "nbody/engines/OpenCLEngine.hpp"
#endif
#if HAVE_OPENMP
#include "nbody/engines/OpenMPEngine.hpp"
#endif
#include "nbody/engines/SingleThreadEngine.hpp"
#include "nbody/engines/SingleThreadUpperEngine.hpp"

TEMPLATE_TEST_CASE(
  "*Engine reproduce same behavior as SingleThreadEngine",
  "[verify][engines][OpenMPEngine][SingleThreadEngine][SingleThreadUpperEngine]",
  #if HAVE_OPENMP
  nbody::engines::OpenMPEngine<double>,
  #endif
  #if HAVE_OPENCL
  nbody::engines::OpenCLEngine<double>,
  #endif
  nbody::engines::SingleThreadUpperEngine<double>
)
{

  double atol = 1e-8;
  double rtol = 1e-5;

  double h_val = 0.0001;
  unsigned steps = 1000;
  unsigned bodies = 13;
  std::vector<double> masses(bodies);
  std::vector<nbody::Vector<double>> pos_ref(bodies);
  std::vector<nbody::Vector<double>> vel_ref(bodies);

  nbody::util::generate_masses(masses);
  nbody::util::generate_initial_conditions(pos_ref, vel_ref);

  std::vector<nbody::Vector<double>> pos_test(pos_ref);
  std::vector<nbody::Vector<double>> vel_test(vel_ref);

  nbody::solvers::RungeKutta<2, nbody::Vector<double>> solver(h_val, masses.size());

  nbody::engines::SingleThreadEngine<double> engine_ref (masses);
  auto pos_updates_ref = [&engine_ref] (auto ... as) {
    engine_ref.position_updates(as...);
  };
  auto vel_updates_ref = [&engine_ref] (auto ... as) {
    engine_ref.velocity_updates(as...);
  };

  TestType engine_test (masses);
  auto pos_updates_test = [&engine_test] (auto ... as) {
    engine_test.position_updates(as...);
  };
  auto vel_updates_test = [&engine_test] (auto ... as) {
    engine_test.velocity_updates(as...);
  };

  unsigned nclose;
  for (unsigned istep=0; istep<steps; istep++) {

    solver(pos_updates_ref, vel_updates_ref, pos_ref.data(), vel_ref.data());
    solver(pos_updates_test, vel_updates_test, pos_test.data(), vel_test.data());

    nclose = 0;
    for (unsigned idx=0; idx<bodies; idx++) {
      bool isclose = nbody::util::isclose(pos_ref[idx], pos_test[idx], rtol, atol) &&
        nbody::util::isclose(vel_ref[idx], vel_test[idx], rtol, atol);
      if (isclose) {
        nclose++;
      } else {
        std::cerr << pos_ref[idx] << std::endl;
        std::cerr << pos_test[idx] << std::endl;
        std::cerr << std::endl;
        std::cerr << vel_ref[idx] << std::endl;
        std::cerr << vel_test[idx] << std::endl;
        std::cerr << std::endl;
      }
    }
    if (nclose != bodies) {
      std::cerr << "istep=" << istep << std::endl;
    }
    REQUIRE(nclose == bodies);
  }

}
