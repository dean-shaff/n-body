#include <sstream>

#include "catch.hpp"

#include "nbody/Vector.hpp"

TEMPLATE_TEST_CASE (
  "Vector behaves as expected",
  "[Vector][unit]",
  float, double
)
{
  SECTION ("Basic constructor works")
  {
    nbody::Vector<TestType> vec;
    REQUIRE(vec.x == 0.0);
    REQUIRE(vec.y == 0.0);
    REQUIRE(vec.z == 0.0);
  }

  SECTION ("Initialization constructor works") {
    nbody::Vector<TestType> vec(1.0, 2.0, 3.0);
    REQUIRE(vec.x == 1.0);
    REQUIRE(vec.y == 2.0);
    REQUIRE(vec.z == 3.0);
  }

  SECTION ("operator+ work as expected") {
    nbody::Vector<TestType> vec(1.0, 2.0, 3.0);

    nbody::Vector<TestType> result = vec + vec;

    CHECK(result.x == 2.0);
    CHECK(result.y == 4.0);
    CHECK(result.z == 6.0);

    result = 1.0 + vec;

    CHECK(result.x == 2.0);
    CHECK(result.y == 3.0);
    CHECK(result.z == 4.0);

    result = vec + 1.0;

    CHECK(result.x == 2.0);
    CHECK(result.y == 3.0);
    CHECK(result.z == 4.0);
  }

  SECTION ("operator- work as expected") {
    nbody::Vector<TestType> vec_lhs(1.0, 2.0, 3.0);
    nbody::Vector<TestType> vec_rhs(1.0, 1.0, 1.0);

    nbody::Vector<TestType> result = vec_lhs - vec_rhs;

    CHECK(result.x == 0.0);
    CHECK(result.y == 1.0);
    CHECK(result.z == 2.0);

    result = vec_lhs - 1.0;

    CHECK(result.x == 0.0);
    CHECK(result.y == 1.0);
    CHECK(result.z == 2.0);
  }

  SECTION ("operator* works as expected") {
    nbody::Vector<TestType> vec_lhs(1.0, 2.0, 3.0);
    nbody::Vector<TestType> vec_rhs(2.0, 3.0, 3.0);

    nbody::Vector<TestType> result = vec_lhs * vec_rhs;

    CHECK(result.x == 2.0);
    CHECK(result.y == 6.0);
    CHECK(result.z == 9.0);

    result = vec_lhs * 10.0;

    CHECK(result.x == 10.0);
    CHECK(result.y == 20.0);
    CHECK(result.z == 30.0);

    result = 10.0 * vec_lhs;

    CHECK(result.x == 10.0);
    CHECK(result.y == 20.0);
    CHECK(result.z == 30.0);
  }

  SECTION ("operator/ works as expected") {
    nbody::Vector<TestType> vec_lhs(1.0, 2.0, 3.0);
    nbody::Vector<TestType> vec_rhs(2.0, 2.0, 3.0);

    nbody::Vector<TestType> result = vec_lhs / vec_rhs;

    CHECK(result.x == 0.5);
    CHECK(result.y == 1.0);
    CHECK(result.z == 1.0);

    result = vec_lhs / 10.0;

    CHECK(result.x == static_cast<TestType>(0.1));
    CHECK(result.y == static_cast<TestType>(0.2));
    CHECK(result.z == static_cast<TestType>(0.3));
  }


  SECTION ("operator<< works as expected") {
    nbody::Vector<TestType> vec(1.0, 2.0, 3.0);
    std::ostringstream oss;
    oss << vec;
    REQUIRE(oss.str() == "[1, 2, 3]");
  }





}
