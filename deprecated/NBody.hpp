#ifndef __NBody_hpp
#define __NBody_hpp

#include <cstring>
#include <vector>

#include "nbody/Vector.hpp"
#include "nbody/engines/SingleThreadEngine.hpp"

namespace nbody {

template<
  typename T,
  typename EngineType=nbody::engines::SingleThreadEngine<T>
>
class NBody {
public:

  NBody (const std::vector<T>& _masses) : masses(_masses) {
    // std::cerr << "NBody::NBody (const std::vector<T>&)" << std::endl;
    n_masses = _masses.size();
    engine = new EngineType(this->masses);
  }

  NBody (const NBody& _nbody) {
    // std::cerr << "NBody::NBody (const NBody& _nbody)" << std::endl;
    masses = _nbody.masses;
    n_masses = _nbody.n_masses;
    engine = new EngineType(this->masses);
  }

  NBody& operator=(const NBody& _nbody) {
    // std::cerr << "NBody::operator=" << std::endl;
    if (&_nbody != this) {
      NBody _nbody;
    }
    return *this;
  }

  ~NBody () {
    // std::cerr << "NBody::~NBody" << std::endl;
    delete engine;
  }


  void operator() (nbody::Vector<T>* current, nbody::Vector<T>* updates) {
    engine->single_step(current, updates);
  }

  const EngineType* get_engine () const { return engine; }
  // void set_engine (EngineType* _engine) {
  //   engine = _engine;
  //   engine->set_masses(masses);
  // }

  unsigned get_n_masses () const { return n_masses; }

  std::vector<T>& get_masses () const { return masses; }

  void set_masses (const std::vector<T>& _masses) {
    masses = _masses;
    n_masses = _masses.size();
  }

private:

  EngineType* engine;

  unsigned n_masses;

  std::vector<T> masses;
};


}



#endif
