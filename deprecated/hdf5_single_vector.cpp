#include <iostream>
#include <string>
#include <vector>

#include "H5Cpp.h"

template<typename T>
struct hdf5_type_map;

template<>
struct hdf5_type_map<int> {
  static H5::PredType get_type () { return H5::PredType::NATIVE_INT; }
};

// template<>
// struct hdf5_type_map<int> {
//   constexpr static H5::PredType type = H5::PredType::NATIVE_INT;
// };


template<typename T>
void write_vector (const std::vector<T>& vec, const std::string& file_name)
{



  H5::H5File* file = new H5::H5File(file_name, H5F_ACC_TRUNC);

  /*
   * Create dataspace for the dataset in the file.
   */
  hsize_t fdim[] = {vec.size()}; // dim sizes of ds (on disk)
  H5::DataSpace dspace(1, fdim);
  /*
   * Create dataset and write it into the file.
   */
  H5::DataSet* dataset = new H5::DataSet(file->createDataSet(
      "vector", hdf5_type_map<T>::get_type(), dspace));
  /*
   * Select hyperslab for the dataset in the file, using 3x2 blocks,
   * (4,3) stride and (2,4) count starting at the position (0,1).
   */
  // hsize_t start[1]; // Start of hyperslab
  // hsize_t stride[1]; // Stride of hyperslab
  // hsize_t count[1];  // Block count
  // hsize_t block[1];  // Block sizes
  // start[0]  = 0;
  // stride[0] = 1;
  // count[0]  = vec.size();
  // block[0]  = 1;
  // dspace.selectHyperslab(H5S_SELECT_SET, count, start, stride, block);

  dataset->write(vec.data(), hdf5_type_map<T>::get_type(), dspace );

  delete dataset;
  delete file;
}


int main ()
{
  std::vector<int> vec(10, 1);
  write_vector(vec, "ones.hdf5");
  return 0;
}
