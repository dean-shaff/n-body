#ifndef __Vector_hpp
#define __Vector_hpp

#include <iostream>

namespace nbody {
  template<typename T>
  struct Vector{
    T x;
    T y;
    T z;

    T x1;
    T y1;
    T z1;

    Vector () {
      x = 0.0;
      y = 0.0;
      z = 0.0;

      x1 = 0.0;
      y1 = 0.0;
      z1 = 0.0;
    }

    Vector (T _x, T _y, T _z) : x(_x), y(_y), z(_z) {
      x1 = 0.0;
      y1 = 0.0;
      z1 = 0.0;
    }

    Vector (T _x, T _y, T _z, T _x1, T _y1, T _z1) : x(_x), y(_y), z(_z), x1(_x1), y1(_y1), z1(_z1) {}

    Vector& operator= (const Vector& _vec) {
      if (&_vec != this) {
        x = _vec.x;
        y = _vec.y;
        z = _vec.z;

        x1 = _vec.x1;
        y1 = _vec.y1;
        z1 = _vec.z1;
      }
      return *this;
    }

    friend bool operator== (const Vector& lhs, const Vector& rhs) {
      return (
        lhs.x == rhs.x &&
        lhs.y == rhs.y &&
        lhs.z == rhs.z &&
        lhs.x1 == rhs.x1 &&
        lhs.y1 == rhs.y1 &&
        lhs.z1 == rhs.z1
      );
    }

    friend bool operator!= (const Vector& lhs, const Vector& rhs) {
      return ! lhs == rhs;
    }

    friend Vector operator+ (const Vector& lhs, const Vector& rhs) {
      Vector result;
      result.x = lhs.x + rhs.x;
      result.y = lhs.y + rhs.y;
      result.z = lhs.z + rhs.z;

      result.x1 = lhs.x1 + rhs.x1;
      result.y1 = lhs.y1 + rhs.y1;
      result.z1 = lhs.z1 + rhs.z1;

      return result;
    }

    template<typename U>
    friend Vector operator+ (U lhs, const Vector& rhs) {
      Vector result;
      result.x = lhs + rhs.x;
      result.y = lhs + rhs.y;
      result.z = lhs + rhs.z;

      result.x1 = lhs + rhs.x1;
      result.y1 = lhs + rhs.y1;
      result.z1 = lhs + rhs.z1;

      return result;
    }

    template<typename U>
    friend Vector operator+ (const Vector& lhs, U rhs) { return rhs + lhs; }

    friend Vector operator- (const Vector& lhs, const Vector& rhs) {
      Vector result;
      result.x = lhs.x - rhs.x;
      result.y = lhs.y - rhs.y;
      result.z = lhs.z - rhs.z;

      result.x1 = lhs.x1 - rhs.x1;
      result.y1 = lhs.y1 - rhs.y1;
      result.z1 = lhs.z1 - rhs.z1;

      return result;
    }

    template<typename U>
    friend Vector operator- (const Vector& lhs, U rhs) {
      Vector result;
      result.x = lhs.x - rhs;
      result.y = lhs.y - rhs;
      result.z = lhs.z - rhs;

      result.x1 = lhs.x1 - rhs;
      result.y1 = lhs.y1 - rhs;
      result.z1 = lhs.z1 - rhs;

      return result;
    }

    friend Vector operator* (const Vector& lhs, const Vector& rhs) {
      Vector result;
      result.x = lhs.x * rhs.x;
      result.y = lhs.y * rhs.y;
      result.z = lhs.z * rhs.z;

      result.x1 = lhs.x1 * rhs.x1;
      result.y1 = lhs.y1 * rhs.y1;
      result.z1 = lhs.z1 * rhs.z1;
      return result;
    }

    template<typename U>
    friend Vector operator* (const Vector& lhs, U rhs) {
      Vector result;
      result.x = lhs.x * rhs;
      result.y = lhs.y * rhs;
      result.z = lhs.z * rhs;

      result.x1 = lhs.x1 * rhs;
      result.y1 = lhs.y1 * rhs;
      result.z1 = lhs.z1 * rhs;

      return result;
    }

    template<typename U>
    friend Vector operator* (U lhs, const Vector& rhs) { return rhs*lhs; }


    friend Vector operator/ (const Vector& lhs, const Vector& rhs) {
      Vector result;
      result.x = lhs.x / rhs.x;
      result.y = lhs.y / rhs.y;
      result.z = lhs.z / rhs.z;

      result.x1 = lhs.x1 / rhs.x1;
      result.y1 = lhs.y1 / rhs.y1;
      result.z1 = lhs.z1 / rhs.z1;

      return result;
    }

    template<typename U>
    friend Vector operator/ (const Vector& lhs, U rhs) {
      Vector result;
      result.x = lhs.x / rhs;
      result.y = lhs.y / rhs;
      result.z = lhs.z / rhs;

      result.x1 = lhs.x1 / rhs;
      result.y1 = lhs.y1 / rhs;
      result.z1 = lhs.z1 / rhs;

      return result;
    }


    friend std::ostream& operator<<(std::ostream& os, const Vector& vec) {
      return os << "[" << vec.x << ", " << vec.y << ", " << vec.z << ", "
        << vec.x1 << ", " << vec.y1 << ", " << vec.z1 << "]";
    }

  };



}

#endif
