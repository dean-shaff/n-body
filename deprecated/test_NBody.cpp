#include "catch.hpp"

#include "nbody/NBody.hpp"


TEMPLATE_TEST_CASE (
  "NBody constructors and basic operators work as expected",
  "[NBody][unit]",
  float, double
) {

  SECTION ("Basic constructor works") {
    nbody::NBody<TestType> nbody (std::vector<TestType>(4, 1.0));
    REQUIRE(nbody.get_n_masses() == 4);
  }

  SECTION ("Copy constructor works") {
    nbody::NBody<TestType> nbody (std::vector<TestType>(4, 1.0));
    nbody::NBody<TestType> nbody_copied (nbody);

    REQUIRE(nbody_copied.get_n_masses() == 4);
  }

  SECTION ("Move constructor works") {
    nbody::NBody<TestType> nbody (nbody::NBody<TestType> (std::vector<TestType>(4, 1.0)));
    REQUIRE(nbody.get_n_masses () == 4);
  }

  SECTION ("copy operator works") {
    nbody::NBody<TestType> nbody (std::vector<TestType>(4, 1.0));
    nbody::NBody<TestType> nbody_copied = nbody;
    REQUIRE(nbody.get_n_masses() == 4);
  }

  SECTION ("move operator works") {
    nbody::NBody<TestType> nbody = nbody::NBody<TestType>(std::vector<TestType>(4, 1.0));
    REQUIRE(nbody.get_n_masses() == 4);
  }
}

TEMPLATE_TEST_CASE (
  "NBody::single_step works",
  "[NBody][unit]",
  float, double
)
{

}
