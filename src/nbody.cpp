#include <iostream>
#include <vector>
#include <algorithm>

#include "toml.hpp"

#include "nbody/Vector.hpp"
#include "nbody/solvers/RungeKutta.hpp"
#include "nbody/engines/SingleThreadEngine.hpp"
#include "nbody/io/hdf5.hpp"
#include "nbody/util/create_parser.hpp"


// bool endswith (const std::string& str, const std::string& end) {
//   if (end.size() > str.size()) {
//     return false;
//   }
//   return std::equal(end.rbegin(), end.rend(), str.rbegin());
// }

template<typename SolverType, typename EngineType, typename T>
void solve (
  SolverType& solver,
  EngineType& engine,
  std::vector<nbody::Vector<T>>& pos,
  std::vector<nbody::Vector<T>>& vel,
  std::vector<T>& result
)
{
  auto pos_updates = [&engine] (auto ... as) {
    engine.position_updates(as...);
  };

  auto vel_updates = [&engine] (auto ... as) {
    engine.velocity_updates(as...);
  };

  unsigned n_masses = pos.size();
  unsigned nsteps = result.size() / (6*n_masses);

  unsigned index;
  for (unsigned istep=0; istep<nsteps; istep++) {
    for (unsigned imass=0; imass<n_masses; imass++) {
      index = 6*istep*n_masses + 6*imass;
      result[index] = pos[imass].x;
      result[index + 1] = pos[imass].y;
      result[index + 2] = pos[imass].z;
      result[index + 3] = vel[imass].x;
      result[index + 4] = vel[imass].y;
      result[index + 5] = vel[imass].z;
    }
    solver(
      pos_updates,
      vel_updates,
      pos.data(),
      vel.data()
    );
  }

}

template<typename T>
void load_from_toml (
  const std::string& file_path,
  std::vector<T>& masses,
  std::vector<nbody::Vector<T>>& pos,
  std::vector<nbody::Vector<T>>& vel
)
{
  auto data = toml::parse(file_path);

  masses = toml::find<std::vector<T>>(data, "masses");

  auto pos_2d = toml::find<std::vector<std::vector<T>>>(data, "position");

  auto vel_2d = toml::find<std::vector<std::vector<T>>>(data, "velocity");

  unsigned n_masses = masses.size();

  pos.resize(n_masses);
  vel.resize(n_masses);

  for (unsigned idx=0; idx<n_masses; idx++) {
    pos[idx].x = pos_2d[idx][0];
    pos[idx].y = pos_2d[idx][1];
    pos[idx].z = pos_2d[idx][2];

    vel[idx].x = vel_2d[idx][0];
    vel[idx].y = vel_2d[idx][1];
    vel[idx].z = vel_2d[idx][2];
  }
}

int main (int argc, char *argv[]) {
  using VecStr = std::vector<std::string>;

  cxxopts::Options options(
     "nbody",
     "Run N-Body simulation for given masses and initial condition");

  nbody::util::create_parser(options);

  std::string config_description = "Text file with comma separated masses, "
    "HDF5 file with \"masses\" field, "
    "or TOML file with \"masses\" field";

  std::string init_cond_description = "Text file with comma separated initial conditions, "
    "HDF5 with 2-D \"data\" field, "
    "or TOML file with \"data\" field";

  options.add_options()
    ("c,config", "TOML config file. Any values in config file will override command line values", cxxopts::value<std::string>());

  auto result = options.parse(argc, argv);
  if (result["help"].as<bool>()) {
    std::cerr << options.help() << std::endl;
    return 0;
  }

  std::string config_file_path = result["config"].as<std::string>();

  std::vector<double> masses;
  std::vector<nbody::Vector<double>> pos;
  std::vector<nbody::Vector<double>> vel;

  load_from_toml(config_file_path, masses, pos, vel);

  double h_val = result["step"].as<double>();
  unsigned nsteps = result["time-steps"].as<unsigned>();

  nbody::engines::SingleThreadEngine<double> engine(masses);
  nbody::solvers::RungeKutta<2, nbody::Vector<double>> solver(h_val, masses.size());

  std::vector<double> result_vec (6*nsteps*masses.size());

  solve(solver, engine, pos, vel, result_vec);

  nbody::io::hdf5::save (
    "nbody.hdf5", "data", result_vec.data(), {nsteps, masses.size(), 6});
}
