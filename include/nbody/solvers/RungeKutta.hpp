#ifndef __RungeKutta_hpp
#define __RungeKutta_hpp

#include <iostream>
#include <vector>

namespace nbody {
namespace solvers {

template<int Order, typename DataContainer>
class RungeKuttaBase {
public:

  RungeKuttaBase (double _h_val, unsigned _elem_per_step) : h_val(_h_val), elem_per_step(_elem_per_step) {
    allocate();
  }

  ~RungeKuttaBase () {
    deallocate();
  }

  double get_h_val () const {return h_val;}
  void set_h_val (double _h_val) { h_val = _h_val; }

  unsigned get_elem_per_step () const { return elem_per_step; }
  void set_elem_per_step (unsigned _elem_per_step) {
    if (_elem_per_step != elem_per_step) {
      deallocate();
      allocate();
      elem_per_step = _elem_per_step;
    }
  }

protected:

  double h_val;

  unsigned elem_per_step;

private:
  void allocate () {}

  void deallocate () {}
};




template<int Order, typename DataContainer>
class RungeKutta : RungeKuttaBase<Order, DataContainer> {
  using RungeKuttaBase<Order, DataContainer>::RungeKuttaBase;
};

template<typename DataContainer>
class RungeKutta<1, DataContainer> : RungeKuttaBase<1, DataContainer> {

public:

  RungeKutta (double _h_val, unsigned _elem_per_step) : RungeKuttaBase<1, DataContainer> (_h_val, _elem_per_step) {
    allocate();
  }

  ~RungeKutta () {
    deallocate();
  }

  template<typename FuncType>
  void operator() (FuncType& y_prime, DataContainer* y_val) {
    double h_val = this->h_val;
    unsigned elem_per_step = this->elem_per_step;

    y_prime(y_val, k1);

    for (unsigned idx=0; idx<elem_per_step; idx++) {
      k2_scratch[idx] = y_val[idx] + k1[idx] * h_val/2.0;
    }

    y_prime(k2_scratch, k2);

    for (unsigned idx=0; idx<elem_per_step; idx++) {
      k3_scratch[idx] = y_val[idx] + k2[idx] * h_val/2.0;
    }
    y_prime(k3_scratch, k3);

    for (unsigned idx=0; idx<elem_per_step; idx++) {
      k4_scratch[idx] = y_val[idx] + k3[idx] * h_val;
    }
    y_prime(k4_scratch, k4);

    for (unsigned idx=0; idx<elem_per_step; idx++) {
      y_val[idx] = y_val[idx] + h_val/6.0*(k1[idx] + 2.0*k2[idx] + 2.0*k3[idx] + k4[idx]);
    }

  }

private:

  void allocate () {
    // std::cerr << "RungeKutta<1, T>::allocate" << std::endl;
    unsigned elem_per_step = this->elem_per_step;

    k1 = new DataContainer[elem_per_step];
    k2 = new DataContainer[elem_per_step];
    k3 = new DataContainer[elem_per_step];
    k4 = new DataContainer[elem_per_step];

    k2_scratch = new DataContainer[elem_per_step];
    k3_scratch = new DataContainer[elem_per_step];
    k4_scratch = new DataContainer[elem_per_step];
  }

  void deallocate () {
    delete[] k1;
    delete[] k2;
    delete[] k3;
    delete[] k4;

    delete[] k2_scratch;
    delete[] k3_scratch;
    delete[] k4_scratch;
  }

  DataContainer* k1;
  DataContainer* k2;
  DataContainer* k3;
  DataContainer* k4;

  DataContainer* k2_scratch;
  DataContainer* k3_scratch;
  DataContainer* k4_scratch;
};

template<typename DataContainer>
class RungeKutta<2, DataContainer> : RungeKuttaBase<2, DataContainer> {

public:

  RungeKutta (double _h_val, unsigned _elem_per_step) : RungeKuttaBase<2, DataContainer> (_h_val, _elem_per_step) {
    allocate();
  }

  ~RungeKutta () {
    deallocate();
  }

  template<typename FuncType0, typename FuncType1>
  void operator() (
    FuncType0& y_prime,
    FuncType1& y_d_prime,
    DataContainer* y_val,
    DataContainer* y_prime_val
  ) {
    double h_val = this->h_val;
    unsigned elem = this->elem_per_step;

    y_prime(y_val, y_prime_val, k1);
    y_d_prime(y_val, y_prime_val, k1 + elem);

    for (unsigned idx=0; idx<elem; idx++) {
      k2_scratch[idx] = y_val[idx] + k1[idx] * h_val/2.0;
      k2_scratch[idx + elem] = y_prime_val[idx] + k1[idx + elem] * h_val/2.0;
    }


    y_prime(k2_scratch, k2_scratch + elem, k2);
    y_d_prime(k2_scratch, k2_scratch + elem, k2 + elem);

    for (unsigned idx=0; idx<elem; idx++) {
      k3_scratch[idx] = y_val[idx] + k2[idx] * h_val/2.0;
      k3_scratch[idx + elem] = y_prime_val[idx] + k2[idx + elem] * h_val/2.0;
    }

    y_prime(k3_scratch, k3_scratch + elem, k3);
    y_d_prime(k3_scratch, k3_scratch + elem, k3 + elem);

    for (unsigned idx=0; idx<elem; idx++) {
      k4_scratch[idx] = y_val[idx] + k3[idx] * h_val;
      k4_scratch[idx + elem] = y_prime_val[idx] + k3[idx + elem] * h_val;
    }

    y_prime(k4_scratch, k4_scratch + elem, k4);
    y_d_prime(k4_scratch, k4_scratch + elem, k4 + elem);

    for (unsigned idx=0; idx<elem; idx++) {
      y_val[idx] = y_val[idx] + h_val/6.0*(
        k1[idx] + 2.0*k2[idx] + 2.0*k3[idx] + k4[idx]);
      y_prime_val[idx] = y_prime_val[idx] + h_val/6.0*(
        k1[idx + elem] +
        2.0*k2[idx + elem] +
        2.0*k3[idx + elem] +
        k4[idx + elem]
      );
    }
  }

private:

  void allocate () {
    unsigned elem_per_step = this->elem_per_step;

    k1 = new DataContainer[2*elem_per_step];
    k2 = new DataContainer[2*elem_per_step];
    k3 = new DataContainer[2*elem_per_step];
    k4 = new DataContainer[2*elem_per_step];

    k2_scratch = new DataContainer[2*elem_per_step];
    k3_scratch = new DataContainer[2*elem_per_step];
    k4_scratch = new DataContainer[2*elem_per_step];
  }

  void deallocate () {
    delete[] k1;
    delete[] k2;
    delete[] k3;
    delete[] k4;

    delete[] k2_scratch;
    delete[] k3_scratch;
    delete[] k4_scratch;
  }

  DataContainer* k1;
  DataContainer* k2;
  DataContainer* k3;
  DataContainer* k4;

  DataContainer* k2_scratch;
  DataContainer* k3_scratch;
  DataContainer* k4_scratch;

};


}
}



#endif
