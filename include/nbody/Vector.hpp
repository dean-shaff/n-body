#ifndef __Vector_hpp
#define __Vector_hpp

#include <iostream>

namespace nbody {
  template<typename T>
  struct Vector{
    T x;
    T y;
    T z;

    Vector () {
      x = 0.0;
      y = 0.0;
      z = 0.0;
    }

    Vector (T _x, T _y, T _z) : x(_x), y(_y), z(_z) {}

    Vector& operator= (const Vector& _vec) {
      if (&_vec != this) {
        x = _vec.x;
        y = _vec.y;
        z = _vec.z;
      }
      return *this;
    }

    friend bool operator== (const Vector& lhs, const Vector& rhs) {
      return (
        lhs.x == rhs.x &&
        lhs.y == rhs.y &&
        lhs.z == rhs.z
      );
    }

    friend bool operator!= (const Vector& lhs, const Vector& rhs) {
      return ! lhs == rhs;
    }

    friend Vector operator+ (const Vector& lhs, const Vector& rhs) {
      Vector result;
      result.x = lhs.x + rhs.x;
      result.y = lhs.y + rhs.y;
      result.z = lhs.z + rhs.z;
      return result;
    }

    template<typename U>
    friend Vector operator+ (U lhs, const Vector& rhs) {
      Vector result;
      result.x = lhs + rhs.x;
      result.y = lhs + rhs.y;
      result.z = lhs + rhs.z;
      return result;
    }

    template<typename U>
    friend Vector operator+ (const Vector& lhs, U rhs) { return rhs + lhs; }

    friend Vector operator- (const Vector& lhs, const Vector& rhs) {
      Vector result;
      result.x = lhs.x - rhs.x;
      result.y = lhs.y - rhs.y;
      result.z = lhs.z - rhs.z;
      return result;
    }

    template<typename U>
    friend Vector operator- (const Vector& lhs, U rhs) {
      Vector result;
      result.x = lhs.x - rhs;
      result.y = lhs.y - rhs;
      result.z = lhs.z - rhs;
      return result;
    }

    friend Vector operator* (const Vector& lhs, const Vector& rhs) {
      Vector result;
      result.x = lhs.x * rhs.x;
      result.y = lhs.y * rhs.y;
      result.z = lhs.z * rhs.z;
      return result;
    }

    template<typename U>
    friend Vector operator* (const Vector& lhs, U rhs) {
      Vector result;
      result.x = lhs.x * rhs;
      result.y = lhs.y * rhs;
      result.z = lhs.z * rhs;
      return result;
    }

    template<typename U>
    friend Vector operator* (U lhs, const Vector& rhs) { return rhs*lhs; }


    friend Vector operator/ (const Vector& lhs, const Vector& rhs) {
      Vector result;
      result.x = lhs.x / rhs.x;
      result.y = lhs.y / rhs.y;
      result.z = lhs.z / rhs.z;
      return result;
    }

    template<typename U>
    friend Vector operator/ (const Vector& lhs, U rhs) {
      Vector result;
      result.x = lhs.x / rhs;
      result.y = lhs.y / rhs;
      result.z = lhs.z / rhs;

      return result;
    }

    friend std::ostream& operator<<(std::ostream& os, const Vector& vec) {
      return os << "[" << vec.x << ", " << vec.y << ", " << vec.z << "]";
    }

  };



}

#endif
