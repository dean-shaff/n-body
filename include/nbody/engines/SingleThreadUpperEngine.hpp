#ifndef __SingleThreadUpperEngine_hpp
#define __SingleThreadUpperEngine_hpp

#include <cmath>
// #include "cblas.h"

#include "nbody/engines/NBodyEngine.hpp"

namespace nbody {
namespace engines {

template<typename T>
class SingleThreadUpperEngine : public NBodyEngine<T> {

public:

  using NBodyEngine<T>::NBodyEngine;

  SingleThreadUpperEngine (const std::vector<T>& _masses) : NBodyEngine<T>(_masses) {
    allocate();
  }

  ~SingleThreadUpperEngine () {
    deallocate();
  }

  void velocity_updates (
    nbody::Vector<T>* position,
    nbody::Vector<T>* velocity,
    nbody::Vector<T>* updates
  )
  {
    T divisor;
    // unsigned index;
    // unsigned index_t;

    // unsigned stride_0 = 1;

    for (unsigned idx=0; idx<this->n_masses; idx++) {
      updates[idx].x = 0.0;
      updates[idx].y = 0.0;
      updates[idx].z = 0.0;
    }

    for (unsigned idx=0; idx<this->n_masses; idx++) {
      for (unsigned idy=idx+1; idy<this->n_masses; idy++) {
        // index = idx*3*this->n_masses + 3*idy;
        // index_t = idy*3*this->n_masses + 3*idx;

        // if (idx == idy) {
        //   continue;
        // }
        
        nbody::Vector<T> diff = position[idy] - position[idx];

        divisor = std::pow(
          std::pow(diff.x, 2) + std::pow(diff.y, 2) + std::pow(diff.z, 2), 1.5
        );

        nbody::Vector<T> quot = diff / divisor;

        updates[idx] = updates[idx] + this->masses[idy]*quot;
        updates[idy] = updates[idy] - this->masses[idx]*quot;

        // accelerations[index] = diff.x / divisor;
        // accelerations[index + 1] = diff.y / divisor;
        // accelerations[index + 2] = diff.z / divisor;

        // accelerations[index_t] = -accelerations[index];
        // accelerations[index_t + 1] = -accelerations[index + 1];
        // accelerations[index_t + 2] = -accelerations[index + 2];
      }
    }

    // for (unsigned idx=0; idx<3; idx++) {
    // matrix_vec_mul(
    //   accelerations,
    //   this->n_masses,
    //   this->n_masses,
    //   this->masses.data(),
    //   accel_updates
    // );
    // }

    // for (unsigned idx=0; idx<this->n_masses; idx++) {
    //   updates[idx].x = accel_updates[3*idx];
    //   updates[idx].y = accel_updates[3*idx + 1];
    //   updates[idx].z = accel_updates[3*idx + 2];
    // }

  }


private:

  void matrix_vec_mul (T* mat, const unsigned dim0, const unsigned dim1, T* vec, T* res) {

    // double alpha = 1.0;
    // double beta = 0.0;
    // for (unsigned idx=0; idx<3; idx++) {
    //   cblas_dgemv(
    //     CblasRowMajor,CblasNoTrans,
    //     dim0, dim1, alpha, mat + idx,
    //     dim0, vec + idx, 3,
    //     beta, res + idx, 3);
    // }

    unsigned mat_index = 0;
    for (unsigned idx=0; idx<3*dim0; idx+=3) {
      res[idx] = 0.0;
      res[idx + 1] = 0.0;
      res[idx + 2] = 0.0;
      for (unsigned idy=0; idy < dim1; idy++) {
        res[idx] += (mat[mat_index] * vec[idy]);
        res[idx + 1] += (mat[mat_index + 1] * vec[idy]);
        res[idx + 2] += (mat[mat_index + 2] * vec[idy]);
        mat_index += 3;
      }
    }
  }

  void deallocate () {
    // std::cerr << "SingleThreadUpperEngine::deallocate" << std::endl;
    // delete [] accelerations;
    // delete [] accel_updates;
  }

  void allocate () {
    // std::cerr << "SingleThreadUpperEngine::allocate" << std::endl;
    // accelerations = new T[3*this->n_masses*this->n_masses];
    // accel_updates = new T[3*this->n_masses];
  }

  // T* accelerations; // 3 x n x n matrix that gets updated at every step
  // T* accel_updates; // 3 x n matrix to hold acceleration updates at every step;
};

}
}


#endif
