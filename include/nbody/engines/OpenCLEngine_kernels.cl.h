R"for_c++_include(
__kernel void calculate_accelerations_upper (
  __global const double* positions,
  __global double* accelerations,
  const unsigned n_masses,
  __global const unsigned* index_lookup,
  const unsigned index_lookup_size
)
{
  const size_t global_id_0 = get_global_id(0);
  const size_t global_size_0 = get_global_size(0);

  // if (global_id_0 > index_lookup_size) {
  //   return;
  // }

  size_t mat_index;
  size_t mat_index_T;

  size_t vec_index_x;
  size_t vec_index_y;

  unsigned idx;
  unsigned idy;

  for (unsigned lookup_idx=global_id_0; lookup_idx<index_lookup_size; lookup_idx+=global_size_0) {

    idx = index_lookup[2*lookup_idx];
    idy = index_lookup[2*lookup_idx + 1];

    mat_index = 3*idx*n_masses + 3*idy;
    mat_index_T = 3*idy*n_masses + 3*idx;

    vec_index_x = 3*idx;
    vec_index_y = 3*idy;

    if (idx == idy) {
      accelerations[mat_index] = 0.0;
      accelerations[mat_index + 1] = 0.0;
      accelerations[mat_index + 2] = 0.0;
      return;
    }

    double diff_x = positions[vec_index_y] - positions[vec_index_x];
    double diff_y = positions[vec_index_y + 1] - positions[vec_index_x + 1];
    double diff_z = positions[vec_index_y + 2] - positions[vec_index_x + 2];

    double divisor = pow(
      pow(diff_x, 2) + pow(diff_y, 2) + pow(diff_z, 2), 1.5
    );

    accelerations[mat_index] = diff_x / divisor;
    accelerations[mat_index + 1] = diff_y / divisor;
    accelerations[mat_index + 2] = diff_z / divisor;

    accelerations[mat_index_T] = -accelerations[mat_index];
    accelerations[mat_index_T + 1] = -accelerations[mat_index + 1];
    accelerations[mat_index_T + 2] = -accelerations[mat_index + 2];
  }
}


__kernel void calculate_accelerations (
  __global const double* positions,
  __global double* accelerations,
  const unsigned n_masses
)
{
  const size_t global_id_0 = get_global_id(0);
  const size_t global_id_1 = get_global_id(1);

  if (global_id_0 > n_masses || global_id_1 > n_masses) {
    return;
  }

  const size_t global_size_0 = get_global_size(0);
  const size_t global_size_1 = get_global_size(1);

  size_t mat_index;
  size_t vec_index_x;
  size_t vec_index_y;

  for (size_t idx=global_id_0; idx<n_masses; idx+=global_size_0) {
    for (size_t idy=global_id_1; idy<n_masses; idy+=global_size_1) {

      mat_index = 3*idx*n_masses + 3*idy;
      vec_index_x = 3*idx;
      vec_index_y = 3*idy;

      if (idx == idy) {
        accelerations[mat_index] = 0.0;
        accelerations[mat_index + 1] = 0.0;
        accelerations[mat_index + 2] = 0.0;
        continue;
      }

      double diff_x = positions[vec_index_y] - positions[vec_index_x];
      double diff_y = positions[vec_index_y + 1] - positions[vec_index_x + 1];
      double diff_z = positions[vec_index_y + 2] - positions[vec_index_x + 2];

      double divisor = pow(
        pow(diff_x, 2) + pow(diff_y, 2) + pow(diff_z, 2), 1.5
      );

      accelerations[mat_index] = diff_x / divisor;
      accelerations[mat_index + 1] = diff_y / divisor;
      accelerations[mat_index + 2] = diff_z / divisor;

      // accelerations[mat_index_T] = -accelerations[mat_index];
      // accelerations[mat_index_T + 1] = -accelerations[mat_index + 1];
      // accelerations[mat_index_T + 2] = -accelerations[mat_index + 2];
    }
  }
}

__kernel void matrix3_vec_mul (
  __global const double* accelerations,
  __global const double* masses,
  __global double* accel_updates,
  const unsigned n_masses
)
{
  __local float shared[24];

  const size_t sub_group_size = get_sub_group_size();
  const size_t local_id_0 = get_local_id(0);
  const size_t local_size_0 = get_local_size(0);
  const size_t group_id_0 = get_group_id(0);

  const size_t warp_num = local_id_0 / sub_group_size;
  const size_t warp_lane = local_id_0 % sub_group_size;

  double update[3] = {0.0, 0.0, 0.0};

  for (size_t idx=local_id_0; idx<n_masses; idx+=local_size_0) {
    for (unsigned idy=0; idy<3; idy++) {
      update[idy] += masses[idx] * accelerations[3*n_masses*group_id_0 + 3*idx + idy];
    }
  }

  for (unsigned idy=0; idy<3; idy++) {
    update[idy] = sub_group_reduce_add (update[idy]);
    if (warp_lane == 0) {
      shared[3*warp_num + idy] = update[idy];
    }
    barrier(CLK_LOCAL_MEM_FENCE);
  }

  if (warp_num == 0) {
    for (unsigned idy=0; idy<3; idy++) {
      update[idy] = shared[3*warp_lane + idy];
      update[idy] = sub_group_reduce_add (update[idy]);
    }
    if (warp_lane == 0) {
      for (unsigned idy=0; idy<3; idy++) {
        accel_updates[3*group_id_0 + idy] = update[idy];
      }
    }
  }
}

__kernel void assign_updates (
  __global const double* in,
  __global double* out,
  const unsigned n_masses
)
{
  const size_t global_id_0 = get_global_id(0);
  if (global_id_0 > n_masses) {
    return;
  }
  const size_t index = 3*global_id_0;

  out[index] = in[index];
  out[index + 1] = in[index + 1];
  out[index + 2] = in[index + 2];
}
)for_c++_include"