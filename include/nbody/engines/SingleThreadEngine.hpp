#ifndef __SingleThreadEngine_hpp
#define __SingleThreadEngine_hpp

#include <cmath>

#include "nbody/engines/NBodyEngine.hpp"

namespace nbody {
namespace engines {

template<typename T>
class SingleThreadEngine : public NBodyEngine<T> {

public:

  using NBodyEngine<T>::NBodyEngine;

  void velocity_updates (
    nbody::Vector<T>* position,
    nbody::Vector<T>* velocity,
    nbody::Vector<T>* updates
  )
  {
    T divisor;
    nbody::Vector<T> acceleration;
    nbody::Vector<T> sub;

    for (unsigned idx=0; idx<this->n_masses; idx++) {
      acceleration.x = 0.0;
      acceleration.y = 0.0;
      acceleration.z = 0.0;

      for (unsigned idy=0; idy<this->n_masses; idy++) {
        if (idx == idy) {
          continue;
        }
        sub = position[idy] - position[idx];
        divisor = std::pow(
            std::pow(sub.x, 2) + std::pow(sub.y, 2) + std::pow(sub.z, 2), 1.5);
        // acceleration += (this->masses[idy] * sub / divisor);
        acceleration = acceleration + ((this->masses[idy] / divisor) * sub);
      }
      updates[idx] = acceleration;
    }
  }
};

}
}


#endif
