#ifndef __nbody_engines_OpenCLEngine_hpp
#define __nbody_engines_OpenCLEngine_hpp

#include <string>

#include "nbody/engines/NBodyEngine.hpp"
#include "opencl/bridge/OpenCLBridge.hpp"

const std::string source =
#include "nbody/engines/OpenCLEngine_kernels.cl.h"
;

namespace nbody {
namespace engines {

template<typename T>
class OpenCLEngine : public NBodyEngine<T> {

public:

  OpenCLEngine (const std::vector<T>& _masses) : NBodyEngine<T> (_masses) {
    allocate();
  }

  OpenCLEngine (const OpenCLEngine& _engine) : NBodyEngine<T> (_engine) {
    bridge = _engine.bridge;
  }

  ~OpenCLEngine () {
    deallocate();
  }

  void position_updates (
    nbody::Vector<T>* position,
    nbody::Vector<T>* velocity,
    nbody::Vector<T>* updates
  )
  {
    unsigned n_masses = this->n_masses;
    cl_int n_masses_cl_uint = static_cast<cl_int>(n_masses);

    bridge.memcpy<T>(velocity_dev, (T*) velocity, 3*n_masses);

    assign_updates_kernel(
      velocity_dev,
      updates_dev,
      n_masses_cl_uint
    );

    bridge.memcpy<T>((T*) updates, updates_dev, 3*n_masses);
  }

  void velocity_updates (
    nbody::Vector<T>* position,
    nbody::Vector<T>* velocity,
    nbody::Vector<T>* updates
  )
  {
    unsigned n_masses = this->n_masses;
    cl_uint n_masses_cl_uint = static_cast<cl_uint>(n_masses);
    cl_uint lookup_size_cl_uint = static_cast<cl_uint>(calc_2d_diag_size(n_masses));
    bridge.memcpy<T>(position_dev, (T*) position, 3*n_masses);

    // calculate_accel_kernel(
    //   position_dev,
    //   accel_dev,
    //   n_masses_cl_uint
    // );

    calculate_accel_kernel(
      position_dev,
      accel_dev,
      n_masses_cl_uint,
      index_lookup_dev,
      lookup_size_cl_uint
    );

    // bridge.memcpy<T>(accel_host, accel_dev, 3*n_masses*n_masses);
    //
    // for (unsigned idx=0; idx<n_masses; idx++) {
    //   for (unsigned idy=0; idy<n_masses; idy++) {
    //     std::cerr << accel_host[3*n_masses*idx + 3*idy] << " ";
    //   }
    //   std::cerr << std::endl;
    // }
    // std::cerr << std::endl;

    matrix_vec_mul_kernel(
      accel_dev,
      masses_dev,
      accel_updates_dev,
      n_masses_cl_uint
    );

    assign_updates_kernel(
      accel_updates_dev,
      updates_dev,
      n_masses_cl_uint
    );

    bridge.memcpy<T>((T*) updates, updates_dev, 3*n_masses);
  }

private:

  unsigned calc_2d_diag_size (unsigned dim0) {
    return static_cast<unsigned>((dim0*(dim0 - 1) / 2) + dim0);
  }

  void calc_lookup () {
    unsigned n_masses = this->n_masses;
    unsigned index = 0;
    for (unsigned idx=0; idx<n_masses; idx++) {
      for (unsigned idy=idx; idy<n_masses; idy++) {
        index_lookup[2*index] = idx;
        index_lookup[2*index + 1] = idy;
        index++;
      }
    }
  }

  void deallocate () {
    delete [] accel_host;
    delete [] accel_updates_host;
    delete [] index_lookup;
  }

  void allocate () {
    unsigned n_masses = this->n_masses;
    unsigned lookup_size = calc_2d_diag_size(n_masses);

    bridge = opencl::bridge::OpenCLBridge {source};

    const unsigned max_local_size = 256;
    const unsigned max_local_size_0 = 16;

    unsigned local_size_0 = n_masses;
    unsigned local_num_groups_0 = 1;

    if (n_masses > max_local_size_0) {
      local_size_0 = max_local_size_0;
      local_num_groups_0 = n_masses / max_local_size_0;
    }
    unsigned global_size_0 = local_size_0 * local_num_groups_0;

    // auto kernel = bridge.create_kernel("calculate_accelerations");
    // calculate_accel_kernel = kernel(
    //   {global_size_0, global_size_0}, {local_size_0, local_size_0}
    // );

    unsigned local_num_groups = 1;
    unsigned local_size = lookup_size;
    if (local_size > max_local_size) {
      local_size = max_local_size;
      local_num_groups = lookup_size / max_local_size;
    }

    auto kernel = bridge.create_kernel("calculate_accelerations_upper");
    calculate_accel_kernel = kernel(
      local_size*local_num_groups, local_size
    );

    local_num_groups = 1;
    local_size = n_masses;
    if (n_masses > max_local_size) {
      local_size = max_local_size;
      local_num_groups = n_masses / max_local_size;
    }

    kernel = bridge.create_kernel("matrix3_vec_mul");
    matrix_vec_mul_kernel = kernel(
      n_masses*local_size, local_size
    );

    kernel = bridge.create_kernel("assign_updates");
    assign_updates_kernel = kernel(
      local_num_groups*local_size, local_size
    );

    index_lookup = new unsigned[2*lookup_size];
    accel_host = new T[3*n_masses*n_masses];
    accel_updates_host = new T[3*n_masses];

    accel_dev = bridge.malloc<T>(3*n_masses*n_masses);
    accel_updates_dev = bridge.malloc<T>(3*n_masses);
    position_dev = bridge.malloc<T>(3*n_masses);
    velocity_dev = bridge.malloc<T>(3*n_masses);
    masses_dev = bridge.malloc<T>(n_masses, CL_MEM_READ_ONLY);
    updates_dev = bridge.malloc<T>(3*n_masses);
    index_lookup_dev = bridge.malloc<unsigned>(2*lookup_size, CL_MEM_READ_ONLY);

    calc_lookup ();

    bridge.memcpy<unsigned>(index_lookup_dev, index_lookup, 2*lookup_size);
    bridge.memcpy<T>(masses_dev, this->masses.data(), n_masses);

  }


  opencl::bridge::OpenCLBridge bridge;
  // 3 x n vector
  cl_mem position_dev;
  // 3 x n vector
  cl_mem velocity_dev;
  // 3 x n x n matrix that gets updated at every step, on device
  cl_mem accel_dev;
  // 3 x n matrix to hold acceleration updates at every step, on device
  cl_mem accel_updates_dev;
  cl_mem masses_dev;
  cl_mem updates_dev;
  cl_mem index_lookup_dev;

  T* accel_host;
  T* accel_updates_host;
  unsigned* index_lookup;

  opencl::bridge::OpenCLKernelFunctor<
    opencl::bridge::OpenCLBridge
  > calculate_accel_kernel;

  opencl::bridge::OpenCLKernelFunctor<
    opencl::bridge::OpenCLBridge
  > matrix_vec_mul_kernel;

  opencl::bridge::OpenCLKernelFunctor<
    opencl::bridge::OpenCLBridge
  > assign_updates_kernel;


};

}
}


#endif
