#ifndef __NBodyEngine_hpp
#define __NBodyEngine_hpp

#include <vector>

#include "nbody/Vector.hpp"

namespace nbody {
namespace engines {

template<typename T>
class NBodyEngine {

public:

  NBodyEngine (const std::vector<T>& _masses) : masses(_masses) {
    n_masses = masses.size();
    allocate();
  }

  NBodyEngine (const NBodyEngine& _engine) {
    masses = _engine.masses;
    n_masses = _engine.n_masses;
  }

  NBodyEngine& operator=(const NBodyEngine& _engine) {
    if (&_engine != this) {
      NBodyEngine _engine;
    }
    return *this;
  }

  ~NBodyEngine () {
    deallocate ();
  }

  void velocity_updates (
    nbody::Vector<T>* position,
    nbody::Vector<T>* velocity,
    nbody::Vector<T>* updates
  );

  void position_updates (
    nbody::Vector<T>* position,
    nbody::Vector<T>* velocity,
    nbody::Vector<T>* updates
  )
  {
    for (unsigned idx=0; idx<this->n_masses; idx++) {
      updates[idx].x = velocity[idx].x;
      updates[idx].y = velocity[idx].y;
      updates[idx].z = velocity[idx].z;
    }
  }


  unsigned get_n_masses () const { return n_masses; }

  std::vector<T>& get_masses () const { return masses; }

  void set_masses (const std::vector<T>& _masses) {
    masses = _masses;
    n_masses = _masses.size();
  }

protected:

  void allocate () {
    // std::cerr << "NBodyEngine::allocate" << std::endl;
  }

  void deallocate () {
    // std::cerr << "NBodyEngine::deallocate" << std::endl;
  }

  bool allocated;

  std::vector<T> masses;

  unsigned n_masses;

};

}
}


#endif
