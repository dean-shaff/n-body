#ifndef __nbody_engines_OpenMPEngine_hpp
#define __nbody_engines_OpenMPEngine_hpp

#include <cmath>
#include <omp.h>

#include "nbody/engines/NBodyEngine.hpp"

namespace nbody {
namespace engines {

template<typename T>
class OpenMPEngine : public NBodyEngine<T> {

public:

  using NBodyEngine<T>::NBodyEngine;

  // void position_updates (
  //   nbody::Vector<T>* position,
  //   nbody::Vector<T>* velocity,
  //   nbody::Vector<T>* updates
  // )
  // {
  //   #pragma omp parallel for
  //   for (int idx=0; idx<this->n_masses; idx++) {
  //     updates[idx] = velocity[idx];
  //   }
  // }

  void velocity_updates (
    nbody::Vector<T>* position,
    nbody::Vector<T>* velocity,
    nbody::Vector<T>* updates
  )
  {
    std::vector<nbody::Vector<T>> accelerations(omp_get_max_threads());
    std::vector<T> divisors(omp_get_max_threads());

    #pragma omp parallel for shared(accelerations, divisors)
    for (int idx=0; idx<this->n_masses; idx++) {
      int ithread = omp_get_thread_num();
      nbody::Vector<T> acceleration = accelerations[ithread];
      T divisor = divisors[ithread];

      nbody::Vector<T> position_idx = position[idx];
      acceleration.x = 0.0;
      acceleration.y = 0.0;
      acceleration.z = 0.0;

      for (int idy=0; idy<this->n_masses; idy++) {
        // std::cerr << "ithread=" << ithread << ", idx=" << idx << ", idy=" << idy << std::endl;
        if (idx == idy) {
          continue;
        }
        nbody::Vector<T> sub = position[idy] - position_idx;
        divisor = std::pow(
            std::pow(sub.x, 2) +
            std::pow(sub.y, 2) +
            std::pow(sub.z, 2), 1.5);
        acceleration = acceleration + ((this->masses[idy] / divisor) * sub);
      }
      updates[idx] = acceleration;
    }
  }
};

}
}


#endif
