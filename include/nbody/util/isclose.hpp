#ifndef __nbody_util_isclose_hpp
#define __nbody_util_isclose_hpp

#include <cmath>

#include "nbody/Vector.hpp"

namespace nbody {
namespace util {

template<typename T>
bool isclose (T a, T b, T rtol=1e-5, T atol=1e-8) {
  return std::abs(a - b) <= (atol + rtol * std::abs(b));
}


template<typename T>
bool isclose (nbody::Vector<T> a, nbody::Vector<T> b, T rtol=1e-5, T atol=1e-8) {
  return (
    isclose(a.x, b.x, rtol, atol) &&
    isclose(a.y, b.y, rtol, atol) &&
    isclose(a.z, b.z, rtol, atol)
  );
}

}
}

#endif
