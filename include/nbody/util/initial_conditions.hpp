#ifndef __nbody_util_initial_conditions_hpp
#define __nbody_util_initial_conditions_hpp

#include <vector>

#include "nbody/Vector.hpp"

namespace nbody {
namespace util {

template<typename T>
inline void generate_initial_conditions (
  std::vector<nbody::Vector<T>>& pos,
  std::vector<nbody::Vector<T>>& vel
)
{
  unsigned n_masses = pos.size();

  for (unsigned idx=0; idx<pos.size(); idx++) {
    T idx_T = static_cast<T>(idx);
    T sign = idx % 2 == 0 ? 1.0: -1.0;
    pos[idx] = nbody::Vector<T> (idx, 0.0, 0.0);
    vel[idx] = nbody::Vector<T> (0.0, sign*idx_T, 0.0);
  }
}

template<typename T>
inline void generate_masses (
  std::vector<T>& masses
)
{
  for (auto it=masses.begin(); it!=masses.end(); it++) {
    *it = 1.0;
  }
}

}
}

#endif
