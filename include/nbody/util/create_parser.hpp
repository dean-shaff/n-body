#ifndef __nbody_util_create_parser_hpp
#define __nbody_util_create_parser_hpp

#include <string>
#include <vector>

#include "nbody/util/cxxopts.hpp" // header file for nice C++ command line options

namespace nbody {
namespace util {

inline void create_parser (cxxopts::Options& options)
{
  options.add_options()
    ("v,verbose", "Verbose output")
    ("h,help", "Help")
    ("t,time-steps", "time steps", cxxopts::value<unsigned>()->default_value("1000"))
    ("s,step", "step size for solver", cxxopts::value<double>()->default_value("0.0001"))
    ("e,engine", "Computation engine", cxxopts::value<std::vector<std::string>>()->default_value({}));
}

}
}



#endif
