#ifndef __nbody_util_bench_hpp
#define __nbody_util_bench_hpp

#include <chrono>

namespace nbody {
namespace util {

  using duration = std::chrono::duration<double, std::ratio<1>>;

  inline std::chrono::time_point<std::chrono::high_resolution_clock> now () {
    return std::chrono::high_resolution_clock::now();
  }

}
}

#endif
