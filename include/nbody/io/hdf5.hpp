#include <iostream>
#include <initializer_list>

#include "H5Cpp.h"

namespace nbody {
namespace io {
namespace hdf5 {



template<typename T>
struct hdf5_type_map;

template<>
struct hdf5_type_map<int> {
  static H5::PredType get_type () { return H5::PredType::NATIVE_INT; }
};

template<>
struct hdf5_type_map<float> {
  static H5::PredType get_type () { return H5::PredType::NATIVE_FLOAT; }
};

template<>
struct hdf5_type_map<double> {
  static H5::PredType get_type () { return H5::PredType::NATIVE_DOUBLE; }
};


template<typename T>
void save (
  const std::string& file_path,
  const std::string& dataset_name,
  const T* result,
  std::initializer_list<hsize_t> _dims
);


template<typename T>
void load (
  const std::string& file_path,
  const std::string& dataset_name,
  T* result,
  std::vector<hsize_t>& dims
);

}
}
}


template<typename T>
void nbody::io::hdf5::save (
  const std::string& file_path,
  const std::string& dataset_name,
  const T* result,
  std::initializer_list<hsize_t> _dims
)
{

  std::vector<hsize_t> dims(_dims.begin(), _dims.end());

  H5::H5File* file = new H5::H5File(file_path, H5F_ACC_TRUNC);

  // hsize_t fdim[] = {time_steps, n_masses, 6}; // dim sizes of ds (on disk)
  H5::DataSpace dspace(dims.size(), dims.data());

  H5::DataSet* dataset = new H5::DataSet(file->createDataSet(
      "data", hdf5_type_map<T>::get_type(), dspace));

  dataset->write(result, hdf5_type_map<T>::get_type(), dspace );
  delete dataset;
  delete file;
}


template<typename T>
void nbody::io::hdf5::load (
  const std::string& file_path,
  const std::string& dataset_name,
  T* result,
  std::vector<hsize_t>& dims
)
{
  H5::H5File* file = new H5::H5File(file_path, H5F_ACC_RDONLY);

  H5::DataSet* dataset = new H5::DataSet(
    file->openDataSet(dataset_name));

  H5::DataSpace dspace = dataset->getSpace();

  int rank = dspace.getSimpleExtentNdims();
  dims.resize(rank);

  int ndims = dspace.getSimpleExtentDims(dims.data(), NULL);

  dataset->read(result, hdf5_type_map<T>::get_type(), dspace);
  delete dataset;
  delete file;
}
