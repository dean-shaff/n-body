#ifndef __opencl_bridge_OpenCLKernel_hpp
#define __opencl_bridge_OpenCLKernel_hpp

#include <string>
#include <initializer_list>

#include "util.hpp"

namespace opencl {
namespace bridge {


template<typename BridgeType>
class OpenCLKernelFunctor {

public:

  OpenCLKernelFunctor () {
  }

  ~OpenCLKernelFunctor () {
  }

  OpenCLKernelFunctor (
    BridgeType* _bridge,
    cl_kernel _kernel,
    const std::vector<size_t>& _global_size,
    const std::vector<size_t>& _local_size
  ) : bridge(_bridge), kernel(_kernel), global_size(_global_size), local_size(_local_size) {
    arg_idx = 0;
  }

  OpenCLKernelFunctor (const OpenCLKernelFunctor& _functor) {
    copy(_functor);
  }

  OpenCLKernelFunctor& operator=(const OpenCLKernelFunctor& _functor) {
    // std::cerr << "OpenCLKernelFunctor::operator=(const OpenCLKernelFunctor&) this=" << this << std::endl;
    if (&_functor != this) {
      copy(_functor);
    }
    return *this;
  }

  void operator() () {
    // std::cerr << "OpenCLKernelFunctor::operator()" << std::endl;
    clErrchk(clEnqueueNDRangeKernel(
      bridge->get_command_queue(), kernel, global_size.size(), NULL,
      global_size.data(), local_size.data(),
      0, NULL, NULL
    ));
    arg_idx = 0;
  }

  template<typename T, typename ... Types>
  void operator() (T arg, Types... args) {
    clErrchk(clSetKernelArg(kernel, arg_idx, sizeof(T), (void*)&arg));
    arg_idx++;
    operator() (args...);
  }

  std::vector<size_t> get_global_size () { return global_size; }
  std::vector<size_t> get_local_size () { return local_size; }

private:

  void copy (const OpenCLKernelFunctor& _functor) {
    bridge = _functor.bridge;
    kernel = _functor.kernel;
    global_size = _functor.global_size;
    local_size = _functor.local_size;
    arg_idx = _functor.arg_idx;
  }

  BridgeType* bridge;
  cl_kernel kernel;

  std::vector<size_t> global_size;
  std::vector<size_t> local_size;

  unsigned arg_idx;
};


template<typename BridgeType>
class OpenCLKernel {

using init_list = std::initializer_list<size_t>;

public:

  OpenCLKernel () {}

  OpenCLKernel (
    BridgeType* _bridge,
    cl_kernel _kernel,
    const std::string& _kernel_name
  ) : bridge(_bridge), kernel(_kernel), kernel_name(_kernel_name)
  { }

  OpenCLKernel (const OpenCLKernel& _kernel) {
    copy(_kernel);
  }

  OpenCLKernel& operator=(const OpenCLKernel& _kernel) {
    if (&_kernel != this) {
      copy(_kernel);
    }
    return *this;
  }

  OpenCLKernelFunctor<BridgeType> operator() (
    init_list global_size, init_list local_size
  )
  {
    std::vector<size_t> global_size_vec (global_size.begin(), global_size.end());
    std::vector<size_t> local_size_vec (local_size.begin(), local_size.end());

    OpenCLKernelFunctor<BridgeType> functor (
      bridge, kernel, global_size_vec, local_size_vec
    );

    return functor;
  }

  OpenCLKernelFunctor<BridgeType> operator() (
    size_t global_size, size_t local_size
  )
  {
    return operator() ({global_size}, {local_size});
  }



private:

  void copy (const OpenCLKernel& _kernel) {
    bridge = _kernel.bridge;
    kernel = _kernel.kernel;
    kernel_name = _kernel.kernel_name;
  }

  std::string kernel_name;

  cl_kernel kernel;

  BridgeType* bridge;

};


}
}



#endif
