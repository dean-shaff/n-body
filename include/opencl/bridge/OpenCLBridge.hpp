#ifndef __opencl_bridge_OpenCLBridge_hpp
#define __opencl_bridge_OpenCLBridge_hpp

#include <iostream>
#include <map>

#include "util.hpp"
#include "OpenCLKernel.hpp"

namespace opencl {
namespace bridge {

class OpenCLBridge {

public:

  OpenCLBridge (const std::string& file_path_or_source)  {
    // std::cerr << "OpenCLBridge::OpenCLBridge (const std::string&)" << std::endl;
    program_source_str = "";
    program_source_file_path = "";
    initialized = false;
    init();
    create_program(file_path_or_source);
  }

  OpenCLBridge () {
    // std::cerr << "OpenCLBridge::OpenCLBridge ()" << std::endl;
    program_source_str = "";
    program_source_file_path = "";
    initialized = false;
  }

  void init () {
    platform_id = NULL;
    device_id = NULL;

    cl_uint ret_num_devices;
    cl_uint ret_num_platforms;
    cl_int error;

    clErrchk(clGetPlatformIDs(1, &platform_id, &ret_num_platforms));
    clErrchk(clGetDeviceIDs(
      platform_id, CL_DEVICE_TYPE_DEFAULT, 1,
      &device_id, &ret_num_devices
    ));
    // Create an OpenCL context
    context = clCreateContext(NULL, 1, &device_id, NULL, NULL, &error);
    clErrchk(error);

    // Create a command queue
    command_queue = clCreateCommandQueueWithProperties(context, device_id, 0, &error);
    clErrchk(error);

    initialized = true;
  }

  OpenCLBridge (const OpenCLBridge& _bridge) {
    // std::cerr << "OpenCLBridge::OpenCLBridge (const OpenCLBridge&)" << std::endl;
    copy (_bridge);
  }

  OpenCLBridge& operator=(const OpenCLBridge& _bridge) {
    // std::cerr << "OpenCLBridge::operator=(const OpenCLBridge& _bridge)" << std::endl;
    if (&_bridge != this) {
      copy (_bridge);
    }
    return *this;
  }


  OpenCLBridge& operator=(OpenCLBridge&& _bridge) {
    // std::cerr << "OpenCLBridge::operator=(OpenCLBridge&& _bridge)" << std::endl;
    if (&_bridge != this) {
      move(_bridge);
    }
    return *this;
  }


  ~OpenCLBridge () {
    if (! initialized) {
      // std::cerr << "OpenCLBridge::~OpenCLBridge () not deleting" << std::endl;
      return;
    } else {
      // std::cerr << "OpenCLBridge::~OpenCLBridge ()" << std::endl;
    }
    clErrchk(clFlush(command_queue));
    clErrchk(clFinish(command_queue));

    for (auto &it : kernels) {
      clErrchk(clReleaseKernel(it.second));
    }

    clErrchk(clRetainProgram(program));

    for (auto &it : mem_vec) {
      clErrchk(clReleaseMemObject(it));
    }

    clErrchk(clReleaseCommandQueue(command_queue));
    clErrchk(clReleaseContext(context));
  }

  void create_program (const std::string& file_path_or_source) {
    // std::cerr << "OpenCLBridge::create_program (const std::string&)"<< std::endl;
    std::ifstream test_file(file_path_or_source);
    if (! test_file) {
      program_source_str = file_path_or_source;
    } else {
      program_source_file_path = file_path_or_source;
      program_source_str = load_file_contents(file_path_or_source);
    }
    // std::cerr << program_source_str << std::endl;
    const char* source_c_str = program_source_str.c_str();
    const size_t source_size = program_source_str.size();

    cl_int error;
    program = clCreateProgramWithSource(
      context, 1,
      (const char **) &source_c_str,
      (const size_t *) &source_size,
      &error
    );

    clErrchk(error);

    error = clBuildProgram(program, 1, &device_id, NULL, NULL, NULL);

    if (error == CL_BUILD_PROGRAM_FAILURE) {
      // Determine the size of the log
      size_t log_size;
      clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);
      // Allocate memory for the log
      std::vector<char> log(log_size);
      // // Get the log
      clGetProgramBuildInfo(
        program, device_id, CL_PROGRAM_BUILD_LOG, log_size, log.data(), NULL);
      // Print the log
      std::string log_str (log.begin(), log.end());
      std::cerr << "build log:" << std::endl;
      std::cerr << log_str << std::endl;
      exit(error);
    }
    clErrchk(error);

  }

  OpenCLKernel<OpenCLBridge> create_kernel (const std::string& kernel_name) {
    cl_kernel kernel;
    if (kernels.find(kernel_name) == kernels.end()) {
      cl_int error;
      kernel = clCreateKernel(program, kernel_name.c_str(), &error);
      clErrchk(error);
      kernels[kernel_name] = kernel;
    } else {
      kernel = kernels[kernel_name];
    }
    return OpenCLKernel<OpenCLBridge> (this, kernel, kernel_name);
  }

  cl_device_type get_device_type () {
    cl_device_type dev_type;
    clErrchk(clGetDeviceInfo(device_id, CL_DEVICE_TYPE, sizeof(dev_type), &dev_type, NULL));
    // if (dev_type == CL_DEVICE_TYPE_cl) {
    //   std::cerr << "cl device" << std::endl;
    // } else if (dev_type == CL_DEVICE_TYPE_CPU) {
    //   std::cerr << "CPU device" << std::endl;
    // } else {
    //   std::cerr << "not sure device device" << std::endl;
    // }
    return dev_type;
  }

  template<typename T>
  cl_mem malloc (size_t size, cl_mem_flags flags=CL_MEM_READ_WRITE) {
    cl_int error;
    cl_mem mem = clCreateBuffer(context, flags, size * sizeof(T), NULL, &error);
    clErrchk(error);
    mem_vec.push_back(mem);
    return mem;
  }

  template<typename T>
  void memcpy (cl_mem dst, T* src, size_t size) {
    clErrchk(
      clEnqueueWriteBuffer(
        command_queue, dst, CL_TRUE, 0,
        size * sizeof(T), src, 0, NULL, NULL
      )
    );
  }

  template<typename T>
  void memcpy (T* dst, cl_mem src, size_t size) {
    clErrchk(
      clEnqueueReadBuffer(
        command_queue, src, CL_TRUE, 0,
        size * sizeof(T), dst, 0, NULL, NULL
      )
    );
  }

  cl_platform_id get_platform_id () { return platform_id; }

  cl_device_id get_device_id () { return device_id; }

  cl_context get_context () { return context; }

  cl_command_queue get_command_queue () { return command_queue; }

  cl_program get_program () { return program; }

  cl_kernel get_kernel (const std::string& kernel_name) { return kernels[kernel_name]; }

private:

  void copy (const OpenCLBridge& _bridge) {
    // std::cerr << "OpenCLBridge::copy (const OpenCLBridge&)" << std::endl;
    if (_bridge.initialized) {
      init();
    }
    program_source_file_path = _bridge.program_source_file_path;
    if (_bridge.program_source_str != "") {
      create_program(_bridge.program_source_str);
    }
    for (auto &it : _bridge.kernels) {
      create_kernel(it.first);
    }
  }

  void move (OpenCLBridge& _bridge) {
    // std::cerr << "OpenCLBridge::move (OpenCLBridge&)" << std::endl;
    if (_bridge.initialized) {
      platform_id = _bridge.platform_id;
      device_id = _bridge.device_id;
      context = _bridge.context;
      command_queue = _bridge.command_queue;
      mem_vec = _bridge.mem_vec;
      program = _bridge.program;
      program_source_str = _bridge.program_source_str;
      program_source_file_path = _bridge.program_source_file_path;
      kernels = _bridge.kernels;
      initialized = _bridge.initialized;
      _bridge.initialized = false;
    }
  }

  cl_platform_id platform_id;
  cl_device_id device_id;
  cl_context context;
  cl_command_queue command_queue;

  std::vector<cl_mem> mem_vec;

  // current program
  cl_program program;
  // current program's source
  std::string program_source_str;
  // current program's source file, if applicable
  std::string program_source_file_path;


  // all kernels that have been generated
  std::map<std::string, cl_kernel> kernels;

  bool initialized;

};

}
}

#endif
